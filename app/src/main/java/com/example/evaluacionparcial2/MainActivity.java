package com.example.evaluacionparcial2;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText txt_nombre,txt_cedula,txt_apellido,txt_fecha,txt_materia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txt_nombre = (EditText) findViewById(R.id.txt_nom);
        txt_cedula = (EditText) findViewById(R.id.txt_cedula);
        txt_apellido = (EditText) findViewById(R.id.txt_ape);
        txt_fecha = (EditText) findViewById(R.id.txt_fecha);
        txt_materia = (EditText) findViewById(R.id.txt_mate);
    }
    public void Guardardatos(View view){
        String cedula = txt_cedula.getText().toString();
        String nombre = txt_nombre.getText().toString();
        String apellido = txt_apellido.getText().toString();
        String fecha = txt_fecha.getText().toString();
        String materia = txt_materia.getText().toString();

        BaseHelper baseHelper = new BaseHelper(this,"DEMODB",null,1);
        SQLiteDatabase db = baseHelper.getWritableDatabase();
        if (db!=null) {
            ContentValues registro = new ContentValues();
            registro.put("Nombre", nombre);
            registro.put("Cedula", cedula);
            registro.put("Apellido", apellido);
            registro.put("Fecha de nacimiento", fecha);
            registro.put("Materia", materia);
            long i = db.insert("Profesores", null, registro);
            if (i>0){
                Toast.makeText(this,"REGISTO YA INSERTADO",Toast.LENGTH_SHORT).show();
            }
        }
    }
}
