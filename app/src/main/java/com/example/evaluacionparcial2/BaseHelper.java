package com.example.evaluacionparcial2;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BaseHelper extends SQLiteOpenHelper {
    String tabla = "CREATE TABLE PROFESOR(Id INTEGER PRIMARY KEY AUTOINCREMENT,Nombre Text,Nombre Text,Apellido Text,Fecha Text,Materia Text )";
    public BaseHelper( Context context,  String name,  SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(tabla);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
